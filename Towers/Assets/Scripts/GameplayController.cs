﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour {

    public Text label;
    void Update()
    {
        if (Globals.towersCount < 100)
            Globals.towersCount = GameObject.FindGameObjectsWithTag("Tower").Length;
        label.text = "Towers: " + Globals.towersCount;
    }
}
