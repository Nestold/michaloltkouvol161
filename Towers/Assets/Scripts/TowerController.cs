﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {
    
    Transform moveObject;
    int bullets = 12;
    void Start()
    {
        Globals.random = new System.Random();
        moveObject = GetComponent<Transform>();
        StartCoroutine(WaitToShoot());
        //StartCoroutine(RotateTower());
        GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
    }

    bool doItOnce = true;

    void Update()
    {
        if(doItOnce && Globals.towersCount >= 100 && bullets == 0)
        {
            bullets = 12;
            GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
            StartCoroutine(RotateTower());
            doItOnce = false;
        }
    }

    IEnumerator RotateTower()
    {
        yield return new WaitForSeconds(0.5f);
        if(bullets > 0)
        {
            var randRotate = Globals.random.Next(15, 45);
            moveObject.Rotate(new Vector3(0, 0, randRotate));
            Fire();
            StartCoroutine(RotateTower());
        }else
        {
            GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
        }
    }

    IEnumerator WaitToShoot()
    {
        yield return new WaitForSeconds(2f);
        StartCoroutine(RotateTower());
    }

    void Fire()
    {
        bullets--;
        var bullet = Instantiate(Resources.Load<GameObject>("Prefabs/Bullet"), moveObject.GetChild(0).localPosition, moveObject.localRotation, transform.parent);
        bullet.transform.localPosition = moveObject.localPosition;
        bullet.GetComponent<BulletController>().StartLife();
    }
}
