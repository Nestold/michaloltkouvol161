﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    bool canDestroy = false;

    Vector3 endPoint;

    public void StartLife()
    {
        endPoint = transform.localPosition + new Vector3(Globals.random.Next(1, 4), Globals.random.Next(1, 4));
        StartCoroutine(DestroyDelay());
        transform.GetComponent<Rigidbody2D>().velocity = transform.up * 4;
    }
    void Update()
    {
        if(Vector3.Distance(transform.localPosition, endPoint) > 4)
        {
            if (Globals.towersCount < 100)
                CreateTower();
            Destroy(gameObject);
        }
    }

    private void CreateTower()
    {
        var tower = Instantiate(Resources.Load<GameObject>("Prefabs/Tower"), transform.parent, false);
        tower.transform.localPosition = transform.localPosition;
    }


    IEnumerator DestroyDelay()
    {
        yield return new WaitForSeconds(0.4f);
        canDestroy = true;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag.Equals("Tower") && canDestroy)
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }
}
