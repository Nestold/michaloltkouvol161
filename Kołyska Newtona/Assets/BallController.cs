﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {
    
    [SerializeField]
    public Direction direction;

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if(Vector2.Distance(transform.position, pos) > 0.01f && Vector2.Distance(transform.position, pos) <= 0.5f)
            {
                switch(direction)
                {
                    case Direction.Left:
                        GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(-100, 0));
                        break;

                    case Direction.Right:
                        GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(100, 0));
                        break;

                    default:
                        Debug.Log("Error Direction");
                        break;
                }
            }
        }
    }
    [SerializeField]
    public enum Direction
    {
        Left,
        Right
    }
}
