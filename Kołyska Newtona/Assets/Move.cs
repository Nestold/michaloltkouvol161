﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public Vector2 maxVel = Vector2.zero;
    public BallController ballController;

    void Start()
    {
        ballController = transform.Find("Ball").GetComponent<BallController>();
    }

    void Update()
    {
        if (maxVel.x < transform.GetComponent<Rigidbody2D>().velocity.x || maxVel.y < transform.GetComponent<Rigidbody2D>().velocity.y)
        {
            if (ballController.direction == BallController.Direction.Right)
                maxVel = -transform.GetComponent<Rigidbody2D>().velocity;
            else
                maxVel = transform.GetComponent<Rigidbody2D>().velocity;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.GetComponent<Move>().ballController.direction == BallController.Direction.Right)
        {
            //col.transform.GetComponent<Move>().maxVel = -maxVel;
            col.transform.GetComponent<Rigidbody2D>().velocity = -maxVel;
        }
        else
        {
            //col.transform.GetComponent<Move>().maxVel = maxVel;
            col.transform.GetComponent<Rigidbody2D>().velocity = maxVel;
        }
        transform.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        maxVel = Vector2.zero;
    }
}
